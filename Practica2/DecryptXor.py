import sys

def obtenerLlave(contend):
   for i in range(len(contend)):
      llave=contend[i]^int("4D",base=16)
      if (contend[i+1]^llave)==int("5A",base=16):
#            print("posible inicio ",i)
            inicio=""
            for j in range(i+64,i+100):
               inicio=inicio+chr(contend[j]^llave)
            #print(inicio+"\n")
            if "This program cannot" in inicio:
               iinicio=i
               break

   print("Byte de inicio .exe: ",iinicio)
   print("Llave int/hex: ",llave,"/",hex(llave))
   return llave,iinicio


def descifrarYQuitarCabecera(contend,llave,iinicio):
   fout=open(sys.argv[2], 'wb')
   for byte in contend[iinicio:]:
      fout.write((byte^llave).to_bytes(1, 'little'))

   print("\nSe obtuvo el archivo",sys.argv[2]+"\n")


def main():
   f=open(sys.argv[1], 'rb')
   contend = f.read()

   llave,iinicio = obtenerLlave(contend)

   descifrarYQuitarCabecera(contend,llave,iinicio)


if(len(sys.argv)==3):
   main()
else:
   print("El programa requiere 2 argumentos.")
   print("DecryptXor.py <Archivo a descifrar> <nombre del archivo de salida>\n")
